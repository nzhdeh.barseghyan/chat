import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxSmartModalModule} from 'ngx-smart-modal';

import {HeaderComponent, FooterComponent, DefaultLayoutComponent, LoginComponent} from '@app/layout';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [HeaderComponent, FooterComponent, DefaultLayoutComponent, LoginComponent],
  imports: [CommonModule, RouterModule, NgxSmartModalModule.forChild()],
  exports: [HeaderComponent, FooterComponent, DefaultLayoutComponent],
  providers: []
})

export class LayoutModule {}
