import {Component, OnInit} from '@angular/core';
import * as _moment from 'moment';
const moment =  _moment;
@Component({
  selector: 'app-footer',
  templateUrl: 'footer.component.html',
  styleUrls: ['footer.component.scss'],
})

export class FooterComponent implements OnInit {
  public currentYear = moment().format('YYYY');
  constructor() {
  }
  ngOnInit(): void {
    console.log(' new one');
  }
}
