import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})

export class HomeComponent implements OnInit {
  public testPage = 'home';
  constructor() {
    console.log(`load a ${this.testPage} page`);
    console.log(' second one');
  }
  ngOnInit(): void {}
}
