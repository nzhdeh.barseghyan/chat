import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home-routing.module';
import {LayoutModule} from '@app/layout/layout.module';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule, HomeRoutingModule, LayoutModule,
  ],
  providers: [],
})

export class HomeModule {}
